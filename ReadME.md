# ServiceJob Spring Boot

This project is a RESTful API for job posting and job application. It is built with Spring Boot framework and uses MySQL as the database.

## API Endpoints

The following are the available endpoints or you can import from `docs/Dans Test Interview Rest Api.postman_collection.json` into Postman:

### Login

This endpoint allows users to login.

- URL: `/user/login`
- Method: `POST`
- Content-Type: `multipart/form-data`
- Body:
  - `email`: Email user `user@email.com`
  - `password`: `halo123@#`
- Response:
  - HTTP Status Code: `200`
  - Body: 
  ```json
  {
    "code": 200,
    "success": "Authorized",
    "message": "Success Login",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjYW5kaWRhdGVAZW1haWwuY29tIiwicm9sZSI6IkNhbmRpZGF0ZSIsImlkZW50aWZ5IjoiMDowOjA6MDowOjA6MDoxIiwiaXNfZW5hYmxlIjp0cnVlLCJpc3MiOiJkZXZlbG9wZXIiLCJleHAiOjE2ODQwODA5NDR9.dUhgl_UaaqzSp953sZzQIm1duvIek1SlfSbKOmrm_dGEsXD1rWxUvCKNFIO81S8gfHAca8c6fioWW_X4vDat4w"
  }
  ```
- Postman Script:
  ```javascript
    var jsonData = JSON.parse(responseBody);
    pm.globals.set("bearer_token",jsonData.token)
  ```

### List Jobs
- URL: `/job/list`
- Method: `GET`
- Headers:
  - Authorization: Bearer {{bearer_token}}
- Response:
  - HTTP Status Code: `200`
  - Body:
  ```json
  {
    "status": "success",
    "code": 200,
    "message": "Data retrieved successfully",
    "data": []
  }
  ```

### List Jobs
- URL: `/job/detail/:id`
- Method: `GET`
- Path Variable: 
  - id: 32bf67e5-4971-47ce-985c-44b6b3860cdb
- Headers:
  - Authorization: Bearer {{bearer_token}}
- Response:
  - HTTP Status Code: `200`
  - Body:
  ```json
  {
    "status": "success",
    "code": 200,
    "message": "Data retrieved successfully",
    "data": []
  }
  ```

## Dependencies

This project uses the following dependencies:

- Spring Web: for creating RESTful APIs
- Spring Data JPA: for working with relational databases
- MySQL Driver: for connecting to MySQL database
- Spring Security: for authentication and authorization
- Auth0 JWT: for JSON Web Token (JWT) authentication

## Configuration

To run this project, the following configuration is required:

- MySQL database
- Java Development Kit (JDK) 17 or later

The following environment variables should be set:

- `DATABASE_URL`: URL to connect to MySQL database
- `DATABASE_USERNAME`: MySQL username
- `DATABASE_PASSWORD`: MySQL password
- `JWT_SECRET`: secret key for generating and validating JWT tokens
- `JWT_EXPIRATION_MS`: expiration time for JWT tokens (in milliseconds)
- `JWT_ISSUER`: for set issuer JWT tokens

## How to Run

To run this project, follow these steps:

1. Clone this repository
2. Set the required environment variables
3. Navigate to the project directory
4. Run the following command: `./mvnw spring-boot:run`

## Usage
- Make the "Login" request to authenticate and obtain the bearer token.
- Use the bearer token in the headers of the "List Jobs" and "Detail Jobs" requests to access protected resources.

Note: Ensure that the API server is running on localhost with the specified ports and paths for the requests to work correctly.

## Contributors

- [M Lukman](https://gitlab.com/iNitialM503) - creator and maintainer