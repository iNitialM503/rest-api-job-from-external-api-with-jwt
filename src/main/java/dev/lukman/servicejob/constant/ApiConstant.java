package dev.lukman.servicejob.constant;

public class ApiConstant {
    public static final String API_USER = "/user";
    public static final String API_JOB = "/job";
    public static final String API_LIST_JOB = "/list";
    public static final String API_DETAIL_JOB = "/detail/{id}";

}
