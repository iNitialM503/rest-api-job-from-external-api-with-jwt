package dev.lukman.servicejob.filter;

import dev.lukman.servicejob.model.CustomUserDetail;
import dev.lukman.servicejob.service.UserService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static dev.lukman.servicejob.filter.KeyConstant.*;


public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
    private final AuthenticationManager authenticationManager;

    private final UserService userService;

    public CustomAuthenticationFilter(AuthenticationManager authenticationManager, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(email, password);
        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        CustomUserDetail auth = (CustomUserDetail) authentication.getPrincipal();
        Algorithm accessTokenAlgorithm = Algorithm.HMAC512(JWT_SECRET.getBytes());
        if (!auth.getUser().isEnabled()) {
            throw new InternalAuthenticationServiceException("User is disabled");
        }else{
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            String accessToken = JWT.create()
                    .withSubject(auth.getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + Long.parseLong(JWT_EXPIRATION_MS))) // 1 Hours
                    .withIssuer(ISSUER)
                    .withClaim("is_enable", auth.getUser().isEnabled())
                    .withClaim("role", auth.getUser().getRole())
                    .withClaim("identify", ipAddress)
                    .sign(accessTokenAlgorithm);
            Map<String, Object> map = new HashMap<>();
            map.put("success", "Authorized");
            map.put("code", HttpStatus.OK.value());
            map.put("message", "Success Login");
            map.put("token", accessToken);
            response.setContentType("application/json");
            new ObjectMapper().writeValue(response.getOutputStream(), map);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        response.setHeader("error", failed.getMessage());
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.setContentType("application/json");
        Map<String, String> error = new HashMap<>();
        if (failed.getCause() instanceof InternalAuthenticationServiceException){
            error.put("message", failed.getMessage());
        } else {
            error.put("message", failed.getMessage());
        }
        new ObjectMapper().writeValue(response.getOutputStream(), error);
    }
}
