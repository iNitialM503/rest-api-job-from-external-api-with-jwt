package dev.lukman.servicejob.filter;

import org.springframework.beans.factory.annotation.Value;

public class KeyConstant {

    public static final String ISSUER = "M Lukman";

    public static final String JWT_SECRET = "Secret@123";

    public static final String JWT_EXPIRATION_MS = "360000";
}
