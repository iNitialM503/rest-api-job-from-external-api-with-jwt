package dev.lukman.servicejob.controller;

import dev.lukman.servicejob.constant.ApiConstant;
import dev.lukman.servicejob.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstant.API_USER)
@RequiredArgsConstructor
public class UserController extends ApiConstant {

    private final UserService userService;
}
