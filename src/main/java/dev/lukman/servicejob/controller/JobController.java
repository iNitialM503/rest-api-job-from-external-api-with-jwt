package dev.lukman.servicejob.controller;

import dev.lukman.servicejob.api.ErrorResponse;
import dev.lukman.servicejob.api.ResponseApi;
import dev.lukman.servicejob.constant.ApiConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(ApiConstant.API_JOB)
@RequiredArgsConstructor
public class JobController {

    @GetMapping("/list")
    public ResponseEntity<Object> getJobs() {
        String apiUrl = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
        return getObjectResponseEntity(apiUrl);
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getDetailJobs(@PathVariable String id) {
        String apiUrl = "http://dev3.dansmultipro.co.id/api/recruitment/positions/" + id;
        return getObjectResponseEntity(apiUrl);
    }

    private ResponseEntity<Object> getObjectResponseEntity(String apiUrl) {
        RestTemplate restTemplate = new RestTemplate();
        try{
            Object data = restTemplate.getForObject(apiUrl, Object.class);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(ResponseApi.success("success", 200, "Data retrieved successfully", data));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResponse("error", HttpStatus.BAD_REQUEST.value(), e.getMessage()));
        }
    }
}
